import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Row, Col, Container, Card, ListGroup, Navbar, Nav, Form } from 'react-bootstrap';
import { reactLocalStorage } from 'reactjs-localstorage';

import { Link, withRouter } from "react-router-dom";

//authentication
import AuthIndexLogin from './Views/Authentication/Login'

//commons
import MenuGenerate from './Views/MenuGenerate'

//system admin
import ManageAuthUsers from './Views/ManagerAdmin/ManageAuthUsers/Index'

import ManageDoctors from './Views/ManagerAdmin/ManageDoctors/Index'
import ManagerAddDoctors from './Views/ManagerAdmin/ManageDoctors/Add'
import ManagerViewDoctor from './Views/ManagerAdmin/ManageDoctors/View'
import ManagerViewDoctorPayments from './Views/ManagerAdmin/ManageDoctors/ViewPayments'

import ManageStaff from './Views/ManagerAdmin/ManageStaff/Index'
import ManagerAddStaff from './Views/ManagerAdmin/ManageStaff/Add'
import ManagerViewStaff from './Views/ManagerAdmin/ManageStaff/View'
import ManagerViewStaffPayments from './Views/ManagerAdmin/ManageStaff/ViewPayments'

import WeeklyIncome from './Views/ManagerAdmin/Report/WeeklyIncome'


//front office clerk
import ManagePatients from './Views/FrontOfficeClerk/Patients/Index'
import ManagePatientsAdd from './Views/FrontOfficeClerk/Patients/Add'
import ManagePatientsView from './Views/FrontOfficeClerk/Patients/View'
import ManageAppointments from './Views/FrontOfficeClerk/Appointments/Index'
import FrontOfficeClerkViewAppointments from './Views/FrontOfficeClerk/Appointments/View'


//top level managers
import PatientVisiting from './Views/TopLevel/Reports/PatientVisiting'
import DoctorAppointments from './Views/TopLevel/Reports/DoctorAppointments'
import DrugSummaryChart from './Views/TopLevel/Reports/DrugSummaryChart'
import PatientVisitByGender from './Views/TopLevel/Reports/PatientVisitByGender'
import IncomeSummary from './Views/TopLevel/Reports/IncomeSummary'
import ExpenseSummary from './Views/TopLevel/Reports/ExpenseSummary'

class AppRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: true,
      loading: false,
      user: null
    };
  }
  componentDidMount(){
      let set = this
      let logged = reactLocalStorage.get('logged');
      let role = reactLocalStorage.get('role');
      let email = reactLocalStorage.get('email');
      let id = reactLocalStorage.get('id');
      let name = reactLocalStorage.get('name');
      
      if(logged === 'true'){
        set.setState({ 
          logged: true, role, email, id, name
        })
      } else {
        set.setState({ logged: false, })
        set.props.history.push('/login')
      }
  }

  signOut(){
    reactLocalStorage.remove('logged');
    reactLocalStorage.remove('role');
    reactLocalStorage.remove('email');
    reactLocalStorage.remove('id');
    reactLocalStorage.remove('name');
    window.location.reload();
  }

  render(){
    
    if(this.state.loading == true){
      return(
        <div className="loadingScreen">
          Spin
        </div>
      )
    } else {

      if(!this.state.logged) {
        return ( 
          <Router>
            <Route path="/login" exact component={ AuthIndexLogin } />
          </Router> 
        )
      } else {
        //let role = "manager_admin"
        //let role = "front_office_clerk"
        //let role = "top_level"
        let role = "manager_admin"

        return (
            <div>

              <Navbar bg="dark" variant="dark">
                <Link to="/" className="navbar-brand">Asceso HMS</Link>
                <Nav className="mr-auto">
                  <Link className="nav-link" to="/" >{ this.state.name }</Link>
                  <Link className="nav-link" to="/" onClick={() => this.signOut() }>Log out</Link>
                </Nav>
                <div className="text-secondary">
                  { this.state.role }
                </div>
              </Navbar>

               

              <Container fluid={ true }>
                  <Row>
                      <Col lg={3} className="bg-light sidebar pt-2" >
                          <MenuGenerate role={ this.state.role }/>
                      </Col>
                      <Col>

                        <Route path="/manager-admin/manage-auth-users" exact component={ ManageAuthUsers } />
                        
                        <Route path="/manager-admin/manage-doctors" component={ ManageDoctors } />
                        <Route path="/manager-admin/add-doctor" component={ ManagerAddDoctors } />
                        <Route path="/manager-admin/view-doctor" component={ ManagerViewDoctor } />
                        <Route path="/manager-admin/view-payments" component={ ManagerViewDoctor } />
                        <Route path="/manager-admin/view-doctor-payment-history" component={ ManagerViewDoctorPayments } />
                        

                        <Route path="/manager-admin/manage-staff" component={ ManageStaff } />
                        <Route path="/manager-admin/add-staff" component={ ManagerAddStaff } />
                        <Route path="/manager-admin/view-staff" component={ ManagerViewStaff } />
                        <Route path="/manager-admin/view-staff-payment-history" component={ ManagerViewStaffPayments } />
                        
                        <Route path="/manager-admin/view-weekly-income" component={ WeeklyIncome } />
                        

                        { /* Front office clerk */ }
                        <Route path="/front-office-clerk/patients" component={ ManagePatients } />
                        <Route path="/front-office-clerk/add-patient" component={ ManagePatientsAdd } />
                        <Route path="/front-office-clerk/view-patient" component={ ManagePatientsView } />

                        <Route path="/front-office-clerk/appointments" component={ ManageAppointments } />
                        <Route path="/front-office-clerk/view-appointments" component={ FrontOfficeClerkViewAppointments } />
                        

                        { /*Top Level Routes */}
                        <Route path="/top-level/reports/patient-visiting" component={ PatientVisiting } />
                        <Route path="/top-level/reports/doctor-appointments" component={ DoctorAppointments } />
                        <Route path="/top-level/reports/drug-summary" component={ DrugSummaryChart } />
                        <Route path="/top-level/reports/patient-visiting-by-gender" component={ PatientVisitByGender } />
                        <Route path="/top-level/reports/income-summary" component={ IncomeSummary } />
                        <Route path="/top-level/reports/expense-summary" component={ ExpenseSummary } />
                        



                        <div className="mt-5">
                          <small className="text-secondary">
                            © Asceso Hospital | All Rights Reserved | Hospital MSS Developed by Esoft
                          </small>
                        </div>
                      </Col>
                  </Row>
              </Container>
      
            </div>  
        )
      }

  
    }

    

  }
}

export default withRouter(AppRouter);
import { combineReducers, createStore } from 'redux';

//presist 
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import { 
  cartReducer, 
  guestUserReducer, 
  ProductsReducer,
  cartPriceReducer } from './Redux/Reducers'


export const reducers = combineReducers({
  cartReducer,
  guestUserReducer,
  ProductsReducer,
  cartPriceReducer
});

// store.js
export function configureStore(pReducer, initialState = {}) {
  const store = createStore(pReducer, initialState);
  return store;
}

//presist
const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
};

const pReducer = persistReducer(persistConfig, reducers);

export const store = configureStore(pReducer);
export const persistor = persistStore(store);

// actions.js
/* export const activateGeod = geod => ({
    type: 'ACTIVATE_GEOD',
    geod
  });
  
  export const closeGeod = () => ({
    type: 'CLOSE_GEOD',
  });

 */

//load products 
export const loadProducts= (products)=> {
  return{
      type: 'LOAD_PRODUCTS',
      products 
  }
}

//add to cart action
export const addToCartFromProductList = (item)=> {
  return{
      type: 'ADD_TO_CART_FROM_PRODUCT_LIST',
      item 
  }
}

//add to cart action
export const addToCart= (item)=> {
    return{
        type: 'ADD_TO_CART',
        item 
    }
}

//remove item action
export const removeItem=(id)=>{
    return{
        type: 'REMOVE_ITEM',
        id
    }
}


export const clearCart = () => ({
  type: 'CLEAR_CART',
});

//save guest user local
export const guestUserSave = (address)=>{
  return{
      type: 'SAVE_GUEST_ADDRESS',
      address
  }
}

//save Take away local
export const takeawayUserSave = (address)=>{
  return{
      type: 'SAVE_TAKE_AWAY_INFO',
      address
  }
}







//reducers

const products = {
  products_list:[],
  cart_items: [],
  cartCount: 0,
  total: 0
}

export const ProductsReducer = (state = products, action)=>{
  
  if(action.type === 'LOAD_PRODUCTS') {
    
    return{
      ...state,
      products_list: action.products,
    }
    
  }

  else {
      return state
  }
}





const cartState = {
    cartItems:[],
    cartCount: 0,
    total: 0
}

export const cartReducer= (state = cartState, action)=>{
    if(action.type === 'ADD_TO_CART') {

        let existed_item = state.cartItems.find( item => action.item.id === item.id )
        //let new_items = state.cartItems.filter(item=> action.item.id !== item.id)

        if(existed_item == undefined) {
          let item = action.item
          item.qty = 1

          console.log("new item " + action.item.quantity)

            return{
                ...state,
                cartItems: [...state.cartItems, item, ],
                cartCount : state.cartCount + 1,
                total: state.total + action.item.price
            }

            
        } else {

            if( existed_item.qty >= action.item.quantity) {
              let item = action.item
              item.qty_exceed = true

              return{
                ...state,
              }
            } else {
              existed_item.qty += 1
              return{
                ...state,
                total: state.total + action.item.price
              }
            }
            
            
        }
        
    }
    if(action.type === 'REMOVE_ITEM'){
        let itemToRemove= state.cartItems.find(item=> action.id === item.id)
        let new_items = state.cartItems.filter(item=> action.id !== item.id)

        //calculating the total
        let newTotal = state.total - ( itemToRemove.price * parseInt(itemToRemove.qty) )

        console.log(itemToRemove.qty)

        return{
            ...state,
            cartItems: new_items,
            total: newTotal,
            cartCount : state.cartCount - 1,
        } 
    }
    if(action.type === 'CLEAR_CART'){ 
      return{
        ...state,
        cartItems:[],
        cartCount: 0,
        total: 0
      } 
    }
    else {
        return state
    }
}




const guest = {
  address:[],
  take_away:[]
}

export const guestUserReducer = (state = guest, action)=>{
  if(action.type === 'SAVE_GUEST_ADDRESS') {
    return{
        ...state,
        address: [action.address],
    }
  }
  if(action.type === 'SAVE_TAKE_AWAY_INFO') {
    return{
        ...state,
        take_away: [action.address],
    }
  }
  else {
      return state
  }
}




//save saveDeliveryPrice
export const saveDeliveryPrice = (price)=>{
  return{
      type: 'SAVE_DELIVERY_PRICE',
      price
  }
}

export const saveTotalPrice = (price)=>{
  return{
      type: 'SAVE_TOTAL_PRICE',
      price
  }
}

const cart_price = {
  delivery: [],
  total: []
}

export const cartPriceReducer = (state = cart_price, action)=>{
  if(action.type === 'SAVE_DELIVERY_PRICE') {
    return{
        ...state,
        delivery: [action.price],
    }
  }
  if(action.type === 'SAVE_TOTAL_PRICE') {
    return{
        ...state,
        total: [action.price],
    }
  }
  else {
      return state
  }
}
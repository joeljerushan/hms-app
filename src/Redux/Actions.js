
/* // actions.js
export const activateGeod = geod => ({
    type: 'ACTIVATE_GEOD',
    geod
  });
  
  export const closeGeod = () => ({
    type: 'CLOSE_GEOD',
  });
 */

  //add to cart action
  export const addToCart= (item)=> {
      return{
          type: 'ADD_TO_CART',
          item 
      }
  }

  //remove item action
  export const removeItem=(id)=>{
      return{
          type: 'REMOVE_ITEM',
          id
      }
  }

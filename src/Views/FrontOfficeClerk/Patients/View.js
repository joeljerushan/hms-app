import React, { Component } from 'react';

import { FaUserMd, FaReceipt, FaUserFriends, FaWheelchair } from 'react-icons/fa';

import { Link, withRouter } from "react-router-dom";

import { Row, Col, Table, Button, Form, Card, Modal, Badge} from 'react-bootstrap';

import FormData from 'form-data';
import axios from 'axios';
import { API_URL } from '../../../Config'
import moment from 'moment'

class View extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            single: null,
            doctors: null,
            selected_doctor: 1,
            availability: null,
            selected_room: null,
        };
        this.modalToggle = this.modalToggle.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
    }

    modalToggle(){
        this.setState({ show_modal: !this.state.show_modal })
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
          [name]: value
        });

    }

    handleSelectChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        
        this.getRoomforDoctor(value)
        this.checkAvailability(value)
        this.setState({
            selected_doctor: value
        });

    }

    getRoomforDoctor(doctor_id){
        let set = this
        
        var formData = new FormData(); 
        formData.append('doctor_id', doctor_id);

        //post request for login
        axios({
            method: 'post',
            url: API_URL + 'room/get',
            data: formData,
            headers: { 
                'Content-Type': 'multipart/form-data',
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ selected_room: response.data})
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    checkAvailability(doctor_id){
        let set = this
        //post request for login
        axios({
            method: 'post',
            url: API_URL + 'doctors/appointment/check?doctor_id=' + doctor_id,
            headers: { 
                'Content-Type': 'multipart/form-data',
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ availability: response.data })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    getInfo(id){
        let set = this
        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'patient/get/' + id,
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ single: response.data })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    getDoctors(){
        let set = this

        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'doctors/list',
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ doctors: response.data.doctors })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    componentDidMount(){
        if(this.props.history.location.state.id){
            this.getInfo(this.props.history.location.state.id)
            this.getDoctors()
            this.getRoomforDoctor(1) //pass doctor id
            this.checkAvailability(1) //pass doctor id
        }
    }
     

    render() {
        if(this.state.single == null){
            return(<p></p>)
        } else {
            return(
                <Card>
                    <Card.Body>
                        <Row className="mb-4">
                            <Col lg={9}><h2><FaUserFriends/> Patient - 
                            { this.state.single.name }</h2></Col>
                            <Col lg={3}>
                                <Button variant="success" className="btn-block" onClick={this.modalToggle} >Create Appointment</Button>
                            </Col>
                        </Row>

                        <Row className="mt-3">
                            <Col>
                                <Form.Group>
                                    <Form.Label>Name</Form.Label>
                                    <h3>{ this.state.single.name }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>NIC</Form.Label>
                                    <h3>{ this.state.single.nic || '' }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Gender</Form.Label>
                                    <h3>{ this.state.single.gender }</h3>
                                </Form.Group>
                            </Col>

                        </Row>

                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Address</Form.Label>
                                    <h3>
                                        { this.state.single.address || '' }
                                    </h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Row>
                                        <Col lg={7}>
                                            <Form.Label>Date of birth</Form.Label>
                                            <h3>{ moment(this.state.single.date_of_birth).format('YYYY-MM-DD') }</h3>
                                        </Col>
                                        <Col>
                                            <Form.Label>Age</Form.Label>
                                            <h3>{ this.state.single.age }</h3>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Contact No</Form.Label>
                                    <h3>{ this.state.single.contact }</h3>
                                </Form.Group>
                            </Col>

                        </Row>

                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Guardian Name</Form.Label>
                                    <h3>{ this.state.single.guardian || 'Not provided' }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                
                            </Col>
                            <Col>
                                
                            </Col>

                        </Row>

                        

                    </Card.Body>

                    <Modal show={this.state.show_modal} onHide={this.modalToggle}>
                        <Modal.Header closeButton>
                            <Modal.Title><FaWheelchair /> Make Appointment</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Table>
                                <tbody>
                                    <tr>
                                        <td><strong>Doctor</strong></td>
                                        <td>
                                            {
                                                this.state.doctors ?
                                                <Form.Control 
                                                    as="select"
                                                    onChange={this.handleSelectChange}
                                                    name="selected_doctor">
                                                    <option disabled>Select</option>
                                                    {
                                                        this.state.doctors.map((single, index) => 
                                                            <option key={index} value={single.id}>{ single.name } - { single.fee } LKR</option>
                                                        )
                                                    }
                                                </Form.Control> : ''
                                            }
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Patient</strong>
                                        </td>
                                        <td>
                                            { this.state.single.name } <br/>
                                            <small>{ this.state.single.nic } - { this.state.single.contact }</small>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Token Number</strong></td>
                                        <td>
                                            { 
                                                this.state.availability ? 
                                                <div>
                                                    {
                                                        this.state.availability.token_issued >= 20 ? 
                                                        <Badge variant="danger">Appointments Closed</Badge>  : 
                                                        this.state.availability.token_issued + 1
                                                    }
                                                </div>
                                                :
                                                <div></div>
                                            }
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td><strong>Room</strong></td>
                                        <td>
                                            { this.state.selected_room ? this.state.selected_room.name : '' }
                                        </td>
                                    </tr>

                                </tbody>
                               
                            </Table>
                        </Modal.Body>
                        <Modal.Footer>
                        { 
                            this.state.availability ? 
                            <div>
                                {
                                    this.state.availability.token_issued >= 20 ? 
                                    <Button variant="secondary" disabled>Appointments Closed</Button>  : 
                                    <Button variant="success" onClick={this.modalToggle}>
                                        Make Appointment
                                    </Button>
                                }
                            </div>
                            :
                            <div></div>
                        }
                        </Modal.Footer>
                    </Modal>
                </Card>
            ) 
        }
    }

}

export default View;


import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaBan, FaUserFriends } from 'react-icons/fa';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import axios from 'axios';
import { API_URL } from '../../../Config'

class ManagePatients extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            list: null
        };
    }

    componentDidMount(){
        let set = this

        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'patient/list',
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ list: response.data.patient })
            } else {
                set.setState({
                    list_e: 'No patient found.'
                })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}>
                            <h2><FaUserFriends /> Patients</h2>  
                        </Col>
                        <Col lg={3}>
                            <Link  
                                to="/front-office-clerk/add-patient" 
                                className="btn btn-primary btn-block">Add New Patient</Link>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th># ID</th>
                                    <th>NIC</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th>Contact No</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>

                                {
                                this.state.list != null ? 
                                this.state.list.map((single, index) => 
                                    <tr key={index}>
                                        <td width="70">{ single.id }</td>
                                        <td width="100">{ single.nic }</td>
                                        <td>{ single.name }</td>
                                        <td width="200">{ single.address }</td>
                                        <td width="50">{ single.gender }</td>
                                        <td width="50">{ single.age }</td>
                                        <td width="50">{ single.contact }</td>
                                        <td width="70">
                                            <Link 
                                                to={{
                                                    pathname: "/front-office-clerk/view-patient",
                                                    state: { id: single.id }
                                                }}
                                                className="btn btn-secondary btn-block btn-sm">
                                                <FaEye />
                                            </Link>
                                        </td>
                                    </tr>
                                ) : 
                                <tr>
                                    <td>Loading..</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                }

                            
                               
                            </tbody>
                        </Table>
                        </Col>
                    </Row>




                </Card.Body>
            </Card>
        )
    }

}

export default ManagePatients;


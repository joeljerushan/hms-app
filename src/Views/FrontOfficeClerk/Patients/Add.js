import React, { Component } from 'react';
import DatePicker from "react-datepicker";

import { FaUserFriends } from 'react-icons/fa';

import { Link, withRouter } from "react-router-dom";

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import FormData from 'form-data';
import axios from 'axios';
import { API_URL } from '../../../Config'
import moment from 'moment'

class Add extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            nic: '',
            gender: 'Male',
            address: '',
            dob: new Date(),
            age: 0,
            contact: '',
            guardian: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleBirthDateChange = date => {
        this.setState({ dob: date });
    };

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    validate(){
        let valid
        
        if(this.state.name == ''){
            this.setState({ name_e: "Name required." })
            valid = false
        } else if(this.state.address == ''){
            this.setState({ address_e: "Address Required." })
            valid = false
        } else if(this.state.age == '' || isNaN(this.state.age)){
            this.setState({ age_e: "Age Required." })
            valid = false
        } else if(this.state.contact == '' || isNaN(this.state.contact)){
            this.setState({ contact_e: "Contact Number Required." })
            valid = false
        } else {
            valid = true 
        }

        return valid
    }

    savePatient(){
        let set = this
        let valid = set.validate()

        if(valid == true){ 
            var formData = new FormData(); 
            formData.append('name', this.state.name);
            formData.append('nic', this.state.nic);
            formData.append('gender', this.state.gender);
            formData.append('address', this.state.address);
            formData.append('date_of_birth', moment(this.state.dob).format('YYYY-MM-DD'));
            formData.append('age', this.state.age);
            formData.append('contact', this.state.contact);
            formData.append('guardian', this.state.guardian);

            set.saveRequest(formData)
        } else {
            set.setState({ save_e: 'Fill required fields.'})
        }
    }

    saveRequest(formData){
        let set = this

        axios({
            method: 'post',
            url: API_URL + 'patient/add',
            data: formData,
            headers: { 
                'Content-Type': 'multipart/form-data',
            }
        })
        .then(function (response) {
          try {
                if(response.data){
                    set.props.history.push('/front-office-clerk/patients')
                }
            } catch(e) {
                set.setState({ save_e: "Saved with errors." })
            } 
        })
        .catch(function (error) {
            set.setState({ save_e: "Something wrong" })
        });
    }
     

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaUserFriends/> Add new Patient</h2></Col>
                        <Col lg={3}>
                            <Button variant="success" className="btn-block" 
                            onClick={ () => this.savePatient() }>Save Patient</Button>
                            { this.state.save_e ? <Badge variant="danger">{ this.state.save_e } </Badge> : ''}
                        </Col>
                    </Row>

                    <Row className="mt-3">
                        <Col>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Patient Name"
                                    onChange={this.handleInputChange}
                                    name="name" />
                                    { this.state.name_e ? <Badge variant="danger">{ this.state.name_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>NIC</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Patient's NIC" 
                                    onChange={this.handleInputChange}
                                    name="nic" />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Gender</Form.Label>
                                <Form.Control as="select">
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Address</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Patient address" 
                                    onChange={this.handleInputChange}
                                    name="address" />
                                    { this.state.address_e ? <Badge variant="danger">{ this.state.address_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Row>
                                    <Col lg={6}>
                                        <Form.Label>Date of birth</Form.Label><br/>
                                        <DatePicker
                                            selected={this.state.dob}
                                            onChange={this.handleBirthDateChange}
                                            className="form-control full-width-input"
                                            showMonthDropdown
                                            showYearDropdown
                                            dropdownMode="select"
                                            maxDate={ new Date('2001-01-01')}
                                            minDate={ new Date('1945-01-01')}
                                        />
                                    </Col>
                                    <Col lg={6}>
                                        <Form.Label>Age</Form.Label><br/>
                                        <Form.Group>
                                            <Form.Control 
                                                type="number" 
                                                placeholder="Age" 
                                                onChange={this.handleInputChange}
                                                name="age" />
                                                { this.state.age_e ? <Badge variant="danger">{ this.state.age_e } </Badge> : ''}
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Contact No</Form.Label>
                                <Form.Control 
                                    type="tel" 
                                    placeholder="Patient's contact number" 
                                    onChange={this.handleInputChange}
                                    name="contact" />
                                    { this.state.contact_e ? 
                                        <Badge variant="danger">
                                            { this.state.contact_e } 
                                        </Badge> 
                                        : ''
                                    }
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col lg={4}>
                            <Form.Group>
                                <Form.Label>Guardian Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="" 
                                    onChange={this.handleInputChange}
                                    name="guardian" />
                            </Form.Group>
                        </Col>
                    </Row>


                </Card.Body>
            </Card>
        )
    }

}

export default withRouter(Add);


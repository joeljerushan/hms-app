import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaBan, FaWheelchair } from 'react-icons/fa';

import { Row, Col, Badge, Button, Form, Card, Table, Modal} from 'react-bootstrap';


class ManageAppointments extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
        };
    }

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaWheelchair /> Appointments</h2></Col>
                    </Row>

                    <Row>
                        <Col lg={6}>
                            <div className="text-center p-3 doctorAppointmentBorder">
                                <h2 className="mb-0">Dr. Pragash Maran</h2>
                                <div><small>Consultant</small></div> 
                                <h1 className="text-success">3/20</h1>
                                <Row>
                                    <Col>
                                        <Link to="/front-office-clerk/view-appointments" className="btn-sm btn-primary mt-2 btn-block">View Appointments</Link>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col lg={6}>
                            <div className="text-center p-3 doctorAppointmentBorder">
                                <h2 className="mb-0">Dr. Praneetha</h2>
                                <div><small>Consultant</small></div> 
                                <h1 className="text-warning">17/20</h1>
                                <Row>
                                    <Col>
                                        <Link to="/front-office-clerk/view-appointments" className="btn-sm btn-primary mt-2 btn-block">View Appointments</Link>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col lg={6}>
                            <div className="text-center p-3 doctorAppointmentBorder">
                                <h2 className="mb-0">Dr. Sujana</h2>
                                <div><small>Consultant</small></div> 
                                <h1 className="text-danger">20/20</h1>
                                <Row>
                                    <Col>
                                        <Link to="/front-office-clerk/view-appointments" className="btn-sm btn-primary mt-2 btn-block">View Appointments</Link>
                                    </Col>
                                    <Col><Button variant="danger" disabled className="btn-sm mt-2 btn-block">Appointments Over</Button> </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>

                    

                </Card.Body>
            </Card>
        )
    }

}

export default ManageAppointments;


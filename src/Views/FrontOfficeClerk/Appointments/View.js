import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaUserClock, FaWheelchair } from 'react-icons/fa';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class ViewAppointments extends Component { 
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}>
                            <h2><FaWheelchair /> Appointments of Dr. Pragash Maran</h2>
                        </Col>
                        <Col lg={3} className="text-right">
                            <h2 className="mb-0"><FaUserClock /> 3/20</h2>
                            <small>3 Appointments out of 20 today</small>
                        </Col>
                    </Row>

                    <Row className="mt-5">
                        <Col>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th># ID</th>
                                    <th>Date</th>
                                    <th>Token</th>
                                    <th>Patient NIC</th>
                                    <th>Doctor</th>
                                    <th>Room</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="70">1</td>
                                    <td>2019-12-10</td>
                                    <td>1</td>
                                    <td>912548295V</td>
                                    <td>Dr. Pragash Maran</td>
                                    <td width="50">
                                        3
                                    </td>
                                </tr>
                                <tr>
                                    <td width="70">2</td>
                                    <td>2019-12-10</td>
                                    <td>2</td>
                                    <td>848582849V</td>
                                    <td>Dr. Pragash Maran</td>
                                    <td width="50">
                                        3
                                    </td>
                                </tr>
                                <tr>
                                    <td width="70">3</td>
                                    <td>2019-12-10</td>
                                    <td>3</td>
                                    <td>748582952V</td>
                                    <td>Dr. Pragash Maran</td>
                                    <td width="50">
                                        3
                                    </td>
                                </tr>
                               
                            </tbody>
                        </Table>
                        </Col>
                    </Row>


                </Card.Body>
            </Card>
        )
    }

}

export default ViewAppointments;


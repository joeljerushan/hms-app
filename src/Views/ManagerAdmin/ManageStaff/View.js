import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaUsers, FaReceipt } from 'react-icons/fa';
import { Row, Col, Badge, Button, Form, Card, Table, Modal} from 'react-bootstrap';

import axios from 'axios';
import { API_URL } from '../../../Config'
import moment from 'moment'

class View extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            single: null,
        };
        this.modalToggle = this.modalToggle.bind(this);
    }

    getInfo(id){
        let set = this
        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'staff/get/' + id,
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ single: response.data })
                console.log(response.data)
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    componentDidMount(){
        if(this.props.history.location.state.id){
            this.getInfo(this.props.history.location.state.id)
        }
    }

    modalToggle(){
        this.setState({ show_modal: !this.state.show_modal })
    }

    render() {
        if(this.state.single == null){
            return(<p></p>)
        } else {
            return(
                <Card>
                    <Card.Body>
                        <Row className="mb-4">
                            <Col lg={5}><h2><FaUsers/> View Staff</h2></Col>
                            <Col lg={2}>
                                <Button variant="success" className="btn-block" onClick={this.modalToggle} >Pay Now</Button>
                            </Col>
                            <Col lg={3}>
                                <Link 
                                    to="/manager-admin/view-staff-payment-history" 
                                    className="btn btn-block btn-secondary">Payment history</Link>
                            </Col>
                            <Col lg={2}>
                                <Button variant="danger" className="btn-block">Disable</Button>
                            </Col>
                        </Row>

                        <Row className="mt-3">
                            <Col>
                                <Form.Group>
                                    <Form.Label>Name</Form.Label>
                                    <h3>{ this.state.single.name }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Designation</Form.Label>
                                    <h3>{ this.state.single.designation }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Gender</Form.Label>
                                    <h3>{ this.state.single.gender }</h3>
                                </Form.Group>
                            </Col>

                        </Row>

                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Address</Form.Label>
                                    <h3>{ this.state.single.address }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Date of birth</Form.Label>
                                    <h3>{ moment(this.state.single.date_of_birth).format('YYYY-MM-DD') }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Contact No</Form.Label>
                                    <h3>{ this.state.single.contact }</h3>
                                </Form.Group>
                            </Col>

                        </Row>

                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Department</Form.Label>
                                    <h3>{ this.state.single.department }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Salary</Form.Label>
                                    <h3>{ this.state.single.salary } LKR</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Has Login</Form.Label>
                                    <h3>{ this.state.single.has_auth === 0 ? 'No' : 'Yes' }</h3>
                                </Form.Group>
                            </Col>

                        </Row>



                        {/* <Row>
                            <Col>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Login information</Card.Title>
                                        <Row>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Email</Form.Label>
                                                    <h3>{ this.state.single.email }</h3>
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Role</Form.Label>
                                                    <h3>FOC</h3>
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Password</Form.Label>
                                                    <Form.Control type="password" placeholder="Type a secure password" />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Confirm Password</Form.Label>
                                                    <Form.Control type="password" placeholder="Confirm password" />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg={3}>
                                                <Button variant="info" className="btn-block">Update Password</Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row> */}


                        <Modal show={this.state.show_modal} onHide={this.modalToggle}>
                            <Modal.Header closeButton>
                                <Modal.Title><FaReceipt /> Make payment to Pragash</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Table>
                                    <tr>
                                        <td><strong>Pay to</strong></td>
                                        <td>{ this.state.single.name }</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Payment Month</strong><br/>
                                            <small>Select salary month for this year</small>
                                        </td>
                                        <td>
                                            <Form.Control as="select">
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">Novemebr</option>
                                                <option value="12">December</option>
                                            </Form.Control>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Amount</strong></td>
                                        <td>
                                            <Form.Control type="number" placeholder="LKR" value={ this.state.single.salary } />
                                        </td>
                                    </tr>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                            <Button variant="success" onClick={this.modalToggle}>
                                Pay Now
                            </Button>
                            </Modal.Footer>
                        </Modal>

                    </Card.Body>
                </Card>
            )
        }
    }

}

export default View;


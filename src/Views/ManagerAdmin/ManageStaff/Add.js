import React, { Component } from 'react';
import DatePicker from "react-datepicker";

import { FaUsers } from 'react-icons/fa';
import { Link, withRouter } from "react-router-dom";

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import FormData from 'form-data';
import axios from 'axios';
import { API_URL } from '../../../Config'

import moment from 'moment'

class Add extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            designation: '',
            gender: 'Male',
            address: '',
            dob: new Date('1998-01-01'),
            contact: '',
            salary: '',
            department: 'opd',
            has_login: false,
            role: 'front_office_clerk'
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleBirthDateChange = date => {
        this.setState({ dob: date });
    };

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    validateStaff(){
        let valid
        
        if(this.state.name == ''){
            this.setState({ name_e: "Staff Name required." })
            valid = false
        } else if(this.state.designation == ''){
            this.setState({ designation_e: "Staff designation required." })
            valid = false
        } else if(this.state.address == ''){
            this.setState({ address_e: "Address Required." })
            valid = false
        } else if(this.state.contact == '' || isNaN(this.state.contact)){
            this.setState({ contact_e: "Valid phone number Required." })
            valid = false
        } else if(this.state.salary == '' || isNaN(this.state.salary)){
            this.setState({ salary_e: "Salary Required." })
            valid = false
        } else {
            valid = true 
        }

        return valid
    }

    AddStaff(){
        let set = this
        let valid = set.validateStaff()

        if(valid == true){ 
            if(set.state.has_login){
                let valid_email = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)
                if(this.state.email == ''){
                    this.setState({ email_e: "Email for login Required." })
                } else if(this.state.password == ''){
                    this.setState({ password_e: "Type strong password." })
                } else if(!valid_email) {
                    this.setState({ email_e: "Valid email Required." })
                } else {
                    var formData = new FormData(); 
                    formData.append('name', this.state.name);
                    formData.append('designation', this.state.designation);
                    formData.append('gender', this.state.gender);
                    formData.append('address', this.state.address);
                    formData.append('date_of_birth', moment(this.state.dob).format('YYYY-MM-DD'));
                    formData.append('contact', this.state.contact);
                    formData.append('salary', this.state.salary);
                    formData.append('department', this.state.department);
                    formData.append('has_auth', 1);
                    formData.append('email', this.state.email);
                    formData.append('password', this.state.password);
                    formData.append('role', this.state.role);
                    set.AddStaffRequest(formData)
                }
                
            } else {
                var formData = new FormData(); 
                formData.append('name', this.state.name);
                formData.append('designation', this.state.designation);
                formData.append('gender', this.state.gender);
                formData.append('address', this.state.address);
                formData.append('date_of_birth', moment(this.state.dob).format('YYYY-MM-DD'));
                formData.append('contact', this.state.contact);
                formData.append('salary', this.state.salary);
                formData.append('department', this.state.department);
                formData.append('has_auth', 0);

                set.AddStaffRequest(formData)
            }
        } else {
            set.setState({ save_e: 'Fill required fields.'})
        }
    }
    
    AddStaffRequest(formData){
        let set = this

        axios({
            method: 'post',
            url: API_URL + 'register',
            data: formData,
            headers: { 
                'Content-Type': 'multipart/form-data',
            }
        })
        .then(function (response) {
          try {
                if(response.data){
                    set.props.history.push('/manager-admin/manage-staff')
                }
            } catch(e) {
                set.setState({ save_e: "Saved with errors." })
            } 
        })
        .catch(function (error) {
            set.setState({ save_e: "Your email already registered." })
        });
    }
     

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaUsers /> Add New Staff</h2></Col>
                        <Col lg={3}>
                            <Button variant="success" className="btn-block" 
                            onClick={ () => this.AddStaff() }>Save Staff</Button>
                            { this.state.save_e ? <Badge variant="danger">{ this.state.save_e } </Badge> : ''}
                        </Col>
                    </Row>

                    <Row className="mt-3">
                        <Col>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Staff Name" 
                                    onChange={this.handleInputChange}
                                    name="name" />
                                    { this.state.name_e ? <Badge variant="danger">{ this.state.name_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Designation</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Staff Designation" 
                                    onChange={this.handleInputChange}
                                    name="designation" />
                                    { this.state.designation_e ? 
                                        <Badge variant="danger">{ this.state.designation_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Gender</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Address</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Staff address" 
                                    onChange={this.handleInputChange}
                                    name="address" />
                                    { this.state.address_e ? <Badge variant="danger">{ this.state.address_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Date of birth</Form.Label><br/>
                                <DatePicker
                                    selected={this.state.dob}
                                    onChange={this.handleBirthDateChange}
                                    className="form-control full-width-input"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    maxDate={ new Date('2001-01-01')}
                                    minDate={ new Date('1945-01-01')}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Contact No</Form.Label>
                                <Form.Control 
                                    type="tel" 
                                    placeholder="Staff contact number"
                                    onChange={this.handleInputChange}
                                    name="contact" />
                                    { this.state.contact_e ? <Badge variant="danger">{ this.state.contact_e } </Badge> : ''}
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Salary</Form.Label>
                                <Form.Control 
                                    type="number" 
                                    placeholder="0 LKR"
                                    onChange={this.handleInputChange}
                                    name="salary" />
                                    { this.state.salary_e ? <Badge variant="danger">{ this.state.salary_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Department</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="department">
                                    <option value="opd">OPD</option>
                                    <option value="ward">Ward</option>
                                    <option value="clinic">Clinic</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <div className="mt-4">
                                    <Form.Check 
                                        name="has_login"
                                        type="checkbox"
                                        checked={this.state.has_login}
                                        onChange={this.handleInputChange} 
                                        inline label="Allow Login" />
                                </div>
                            </Form.Group>
                        </Col>

                    </Row>
                    
                    {
                        this.state.has_login ? 
                        <div>
                            <Row>
                                <Col>
                                    <Card>
                                        <Card.Body>
                                            <Card.Title>Login information</Card.Title>
                                            <Row>
                                                <Col>
                                                    <Form.Group>
                                                        <Form.Label>Email</Form.Label>
                                                        <Form.Control 
                                                            type="text" 
                                                            placeholder="Staff Email"
                                                            onChange={this.handleInputChange}
                                                            name="email" />
                                                            { this.state.email_e ? <Badge variant="danger">{ this.state.email_e } </Badge> : ''}
                                                    </Form.Group>
                                                </Col>
                                                <Col>
                                                    <Form.Group>
                                                        <Form.Label>Role</Form.Label>
                                                        <Form.Control 
                                                            as="select"
                                                            onChange={this.handleInputChange}
                                                            name="role">
                                                            <option value="front_office_clerk">Front office clerk</option>
                                                            <option value="manager_admin">Manager Admin</option>
                                                            <option value="technician">Technician</option>
                                                            <option value="pharmacist">Pharmacist</option>
                                                            <option value="top_level_management">Top level management</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Form.Group>
                                                        <Form.Label>Password</Form.Label>
                                                        <Form.Control 
                                                            type="password" 
                                                            placeholder="Type a secure password" 
                                                            onChange={this.handleInputChange}
                                                            name="password" />
                                                            { this.state.password_e ? <Badge variant="danger">{ this.state.password_e } </Badge> : ''}
                                                    </Form.Group>
                                                </Col>
                                                <Col>
                                                    <Form.Group>
                                                        <Form.Label>Confirm Password</Form.Label>
                                                        <Form.Control 
                                                            type="password" 
                                                            placeholder="Confirm password" 
                                                            onChange={this.handleInputChange}
                                                            name="confirm_password" />
                                                            { this.state.confirm_password_e ? <Badge variant="danger">{ this.state.confirm_password_e } </Badge> : ''}
                                                    </Form.Group>
                                                </Col>
                                            </Row>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </div> : ''
                    }
                    


                </Card.Body>
            </Card>
        )
    }

}

export default Add;


import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaUserMd } from 'react-icons/fa';
import { Row, Col, Badge, Button, Form, Card, Table, OverlayTrigger, Tooltip} from 'react-bootstrap';


class ViewPayments extends Component { 
    constructor(props) {
        super(props);
        this.state = {

        };
    }

     

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaUserMd/> Payment history of Tani Masha</h2></Col>
                        <Col lg={3}>
                            
                        </Col>
                    </Row>
                    <small className="text-secondary">To view Reference hover eye (<FaEye />) icon</small>
                    <Table striped bordered hover size="sm" className="mt-3">
                        <thead>
                            <tr>
                                <th># ID</th>
                                <th>Date Paid</th>
                                <th>Period</th>
                                <th>Bank</th>
                                <th>Amount</th>
                                <th>Reference</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-02-07</td>
                                <td width="210">January</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    24300 LKR
                                </td>
                                <td width="70">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-03-07</td>
                                <td width="210">February</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    27100 LKR
                                </td>
                                <td width="70">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-04-07</td>
                                <td width="210">March</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    21700 LKR
                                </td>
                                <td width="50">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-05-07</td>
                                <td width="210">April</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    28500 LKR
                                </td>
                                <td width="50">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>
                        
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        )
    }

}

export default ViewPayments;


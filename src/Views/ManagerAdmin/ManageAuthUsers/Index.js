import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaBan } from 'react-icons/fa';


import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import axios from 'axios';
import { API_URL } from '../../../Config'

class ManageUsers extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            list: null
        };
    }

    componentDidMount(){
        let set = this
        axios({
            method: 'get',
            url: API_URL + 'users/list',
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ list: response.data.users })
            } else {
                set.setState({
                    list_e: 'No Doctors Found.'
                })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    render() {
        return(
            <Card>
                <Card.Body>
                    <Card.Title>Manage Authenticatable Users</Card.Title>

                    <Row>
                        <Col>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th># ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.list != null ? 
                                this.state.list.map((single, index) => 
                                    <tr key={index}>
                                        <td width="50">{ single.id }</td>
                                        <td>{ single.name }</td>
                                        <td width="70">{ single.email }</td>
                                        <td width="200">{ single.role }</td>
                                        <td width="70">
                                            <Link 
                                                to={{
                                                    pathname: "/manager-admin/view-doctor",
                                                    state: { id: single.id }
                                                }}
                                                className="btn btn-secondary btn-block btn-sm">
                                                <FaEye />
                                            </Link>
                                        </td>
                                    </tr>
                                ) : 
                                <tr>
                                    <td>Loading..</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            }
                               
                            </tbody>
                        </Table>
                        </Col>
                    </Row>




                </Card.Body>
            </Card>
        )
    }

}

export default ManageUsers;


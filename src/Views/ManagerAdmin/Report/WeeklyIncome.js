import React, { Component } from 'react';
import DateRangePicker from 'react-bootstrap-daterangepicker';

import { Link, withRouter } from "react-router-dom";
import { FaEye } from 'react-icons/fa';
import { Row, Col, Badge, Button, Form, Card, Table, OverlayTrigger, Tooltip} from 'react-bootstrap';
import moment from 'moment-timezone';

class WeeklyIncome extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            report_range: new Date(),
            start_date: '',
            end_date: ''
        };
    }
     

    render() {
        return(
            <Card>
                <Card.Body>
                    <Card.Title>Weekly Income Report</Card.Title>
                    
                    <Row>
                        <Col lg={2}>
                            <h6 className="mt-2">Select Range</h6>
                        </Col>
                        <Col lg={5}>
                            <DateRangePicker 
                                onApply={( event, picker ) => this.setState({ 
                                    start_date: picker.startDate,
                                    end_date: picker.endDate,
                                })} 
                                className="full-width-input">
                                <Form.Control 
                                    type="text" 
                                    placeholder="Select date range"
                                />
                            </DateRangePicker>

                        </Col>
                        <Col>
                        {
                            this.state.start_date !== '' ?
                            <h4>{ moment(this.state.start_date).format('MM/DD/YYYY') } to { moment(this.state.end_date).format('MM/DD/YYYY') }</h4> : ''
                        }
                        </Col>
                    </Row>

                    {/* <Table striped bordered hover size="sm" className="mt-3">
                        <thead>
                            <tr>
                                <th># ID</th>
                                <th>Date Paid</th>
                                <th>Period</th>
                                <th>Bank</th>
                                <th>Amount</th>
                                <th>Reference</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-02-23</td>
                                <td width="210">2019-01-12 - 2019-01-19</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    8400 LKR
                                </td>
                                <td width="70">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-02-23</td>
                                <td width="210">2019-01-12 - 2019-01-19</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    8400 LKR
                                </td>
                                <td width="70">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td width="50">1</td>
                                <td width="120">2019-02-23</td>
                                <td width="210">2019-01-12 - 2019-01-19</td>
                                <td>
                                    Nations Trust
                                </td>
                                <td width="100">
                                    8400 LKR
                                </td>
                                <td width="50">
                                    <div className="text-center">
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={ <Tooltip>Hello World</Tooltip>}>
                                            <FaEye />
                                        </OverlayTrigger>
                                    </div>
                                </td>
                            </tr>
                        
                        </tbody>
                    </Table> */}
                </Card.Body>
            </Card>
        )
    }

}

export default WeeklyIncome;
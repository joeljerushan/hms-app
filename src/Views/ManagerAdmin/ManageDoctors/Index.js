import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaEye, FaUserMd } from 'react-icons/fa';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import axios from 'axios';
import { API_URL } from '../../../Config'

class Index extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            list: null
        };
    }

    componentDidMount(){
        let set = this

        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'doctors/list',
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ list: response.data.doctors })
            } else {
                set.setState({
                    list_e: 'No Doctors Found.'
                })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}>
                            <h2><FaUserMd/> Manage Doctors</h2>
                        </Col>
                        <Col lg={3}>
                            <Link  
                                to="/manager-admin/add-doctor" 
                                className="btn btn-primary btn-block">Add New Doctor</Link>
                        </Col>
                    </Row>
                


                    <Row>
                        <Col>
                        <Table striped bordered hover size="sm">
                            <thead>
                                <tr>
                                    <th># ID</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Address</th>
                                    <th>Contact</th>
                                    <th>Unit</th>
                                    <th>Category</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.list != null ? 
                                    this.state.list.map((single, index) => 
                                        <tr key={index}>
                                            <td width="50">{ single.id }</td>
                                            <td>{ single.name }</td>
                                            <td width="70">{ single.gender }</td>
                                            <td width="200">{ single.address }</td>
                                            <td width="100">{ single.contact }</td>
                                            <td width="70">{ single.unit_type }</td>
                                            <td width="70">{ single.category }</td>
                                            <td width="70">
                                                <Link 
                                                    to={{
                                                        pathname: "/manager-admin/view-doctor",
                                                        state: { id: single.id }
                                                    }}
                                                    className="btn btn-secondary btn-block btn-sm">
                                                    <FaEye />
                                                </Link>
                                            </td>
                                        </tr>
                                    ) : 
                                    <tr>
                                        <td>Loading..</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                }
                            </tbody>
                        </Table>
                        </Col>
                    </Row>




                </Card.Body>
            </Card>
        )
    }

}

export default Index;


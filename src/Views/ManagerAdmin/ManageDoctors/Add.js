import React, { Component } from 'react';
import DatePicker from "react-datepicker";

import { FaUserMd } from 'react-icons/fa';

import { Link, withRouter } from "react-router-dom";

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import FormData from 'form-data';
import axios from 'axios';
import { API_URL } from '../../../Config'

import moment from 'moment'

class Add extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            nic: '',
            gender: 'Male',
            address: '',
            dob: new Date('1998-01-01'),
            phone: '',
            unit: 'OPD',
            category: 'Consultant',
            qualification: '',
            bank: 'Bank of Ceylon',
            bank_account_no: '',
            fee: '',
            email: '',
            passowrd: '',
            confirm_password: '',
            role: 'doctor',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }
    

    handleChange = date => {
        this.setState({
          dob: date
        });
    };

    validateDoctor(){
        let valid
        let valid_email = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)

        if(this.state.name == ''){
            this.setState({ name_e: "Doctor's Name Required." })
            valid = false
        } else if(this.state.nic == ''){
            this.setState({ nic_e: "Doctor's NIC Required." })
            valid = false
        } else if(this.state.address == ''){
            this.setState({ address_e: "Address Required." })
            valid = false
        } else if(this.state.phone == '' || isNaN(this.state.phone)){
            this.setState({ phone_e: "Valid phone number Required." })
            valid = false
        } else if(this.state.qualification == ''){
            this.setState({ qualification_e: "Qualification Required." })
            valid = false
        } else if(this.state.bank_account_no == ''){
            this.setState({ bank_account_no_e: "Bank account number Required." })
            valid = false
        } else if(this.state.bank_account_no == ''){
            this.setState({ bank_account_no_e: "Bank account number Required." })
            valid = false
        } else if(this.state.fee == '') {
            this.setState({ fee_e: "Doctors Fee Required." })
        } else if(this.state.email == ''){
            this.setState({ email_e: "Email for login Required." })
            valid = false
        } else if(this.state.password == ''){
            this.setState({ password_e: "Type strong password." })
            valid = false
        } else if(!valid_email) {
            this.setState({ email_e: "Valid email Required." })
        }  else {
            valid = true 
        }

        return valid
    }

    AddDoctor(){
        let set = this
        let valid = set.validateDoctor()
        if(valid == true){

            var formData = new FormData(); 
            formData.append('name', this.state.name);
            formData.append('nic', this.state.nic);
            formData.append('gender', this.state.gender);
            formData.append('address', this.state.address);
            formData.append('date_of_birth', moment(this.state.dob).format('YYYY-MM-DD'));
            formData.append('contact', this.state.phone);
            formData.append('unit_type', this.state.unit);
            formData.append('category', this.state.category);
            formData.append('qualification', this.state.qualification);
            formData.append('bank', this.state.bank);
            formData.append('account_number', this.state.bank_account_no);
            formData.append('fee', this.state.fee);
            formData.append('role', 'doctor');
            formData.append('has_auth', 1);
            formData.append('email', this.state.email);
            formData.append('password', this.state.password);

            
            
            axios({
                method: 'post',
                url: API_URL + 'register',
                data: formData,
                headers: { 
                    'Content-Type': 'multipart/form-data',
                }
            })
            .then(function (response) {
              try {
                    if(response.data){
                        set.props.history.push('/manager-admin/manage-doctors')
                    }
                } catch(e) {
                    set.setState({ save_e: "Saved with errors." })
                } 
            })
            .catch(function (error) {
                set.setState({ save_e: "Your email already registered." })
            });

        } else {
            set.setState({ save_e: "Please check all field." })
        }
    }
     

    render() {
        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaUserMd/> Add new Doctor</h2></Col>
                        <Col lg={3}>
                            <Button variant="success" className="btn-block" 
                            onClick={ () => this.AddDoctor() }>Save Doctor</Button>
                            { this.state.save_e ? <Badge variant="danger">{ this.state.save_e } </Badge> : ''}
                        </Col>
                    </Row>
                    
                    <Row className="mt-3">
                        <Col>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Doctor Name"
                                    onChange={this.handleInputChange}
                                    name="name" />
                                { this.state.name_e ? <Badge variant="danger">{ this.state.name_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>NIC</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Doctor's NIC"
                                    onChange={this.handleInputChange}
                                    name="nic" />
                                { this.state.nic_e ? <Badge variant="danger">{ this.state.nic_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Gender</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Address</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Doctor's address"
                                    onChange={this.handleInputChange}
                                    name="address" />
                                { this.state.address_e ? <Badge variant="danger">{ this.state.address_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Date of birth</Form.Label><br/>
                                <DatePicker
                                    selected={this.state.dob}
                                    onChange={this.handleChange}
                                    className="form-control full-width-input"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    maxDate={ new Date('1998-01-01')}
                                    minDate={ new Date('1945-01-01')}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Contact No</Form.Label>
                                <Form.Control 
                                    type="tel" 
                                    placeholder="Doctor's contact number"
                                    onChange={this.handleInputChange}
                                    name="phone" />
                                { this.state.phone_e ? <Badge variant="danger">{ this.state.phone_e } </Badge> : ''}
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Type of unit</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="unit">
                                    <option value="OPD">OPD</option>
                                    <option value="Ward">Ward</option>
                                    <option value="Clinic">Clinic</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Category</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="category">
                                    <option value="Consultant">Consultant</option>
                                    <option value="Register">Register</option>
                                    <option value="Medical Officer">Medical Officer</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Qualification</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Doctor's qualification"
                                    onChange={this.handleInputChange}
                                    name="qualification" />
                                { this.state.qualification_e ? <Badge variant="danger">{ this.state.qualification_e } </Badge> : ''}
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Bank</Form.Label>
                                <Form.Control 
                                    as="select"
                                    onChange={this.handleInputChange}
                                    name="bank">
                                    <option value="Bank of Ceylon">Bank of Ceylon</option>
                                    <option value="Sampath Bank">Sampath Bank</option>
                                    <option value="HNB">HNB</option>
                                    <option value="Nation Trust">Nation Trust</option>
                                    <option value="Commercial Bank">Commercial Bank</option>
                                    <option value="NSB">NSB</option>
                                    <option value="Peoples Bank">Peoples Bank</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Account Number</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Doctor's Bank Account No."
                                    onChange={this.handleInputChange}
                                    name="bank_account_no" />
                                { this.state.bank_account_no_e ? <Badge variant="danger">{ this.state.bank_account_no_e } </Badge> : ''}
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Fee</Form.Label>
                                <Form.Control 
                                    type="number" 
                                    placeholder="Doctor's Fee."
                                    onChange={this.handleInputChange}
                                    name="fee" />
                                { this.state.fee_e ? <Badge variant="danger">{ this.state.fee_e } </Badge> : ''}
                            </Form.Group>
                        </Col>

                    </Row>


                    <Row>
                        <Col>
                            <Card>
                                <Card.Body>
                                    <Card.Title>Login information</Card.Title>
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control 
                                                    type="text" 
                                                    placeholder="Doctor's Email"
                                                    onChange={this.handleInputChange}
                                                    name="email" />
                                                { this.state.email_e ? <Badge variant="danger">{ this.state.email_e } </Badge> : ''}
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Role</Form.Label>
                                                <h4>Doctor</h4>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Password</Form.Label>
                                                <Form.Control 
                                                    type="password" 
                                                    placeholder="Type a secure password"
                                                    onChange={this.handleInputChange}
                                                    name="password" />
                                                { this.state.password_e ? <Badge variant="danger">{ this.state.password_e } </Badge> : ''}
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Confirm Password</Form.Label>
                                                <Form.Control 
                                                    type="password" 
                                                    placeholder="Confirm password"
                                                    onChange={this.handleInputChange}
                                                    name="confirm_password" />
                                                { this.state.confirm_password_e ? <Badge variant="danger">{ this.state.confirm_password_e } </Badge> : ''}
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default withRouter(Add);


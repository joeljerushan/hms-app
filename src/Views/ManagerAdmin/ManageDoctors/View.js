import React, { Component } from 'react';

import { FaUserMd, FaReceipt } from 'react-icons/fa';

import { Link, withRouter } from "react-router-dom";

import { Row, Col, Table, Button, Form, Card, Modal} from 'react-bootstrap';


import axios from 'axios';
import { API_URL } from '../../../Config'
import moment from 'moment'
class View extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            show_modal: false,
            single: null,
        };
        this.modalToggle = this.modalToggle.bind(this);
    }

    modalToggle(){
        this.setState({ show_modal: !this.state.show_modal })
    }

    getInfo(id){
        let set = this
        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'doctors/get/' + id,
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ single: response.data })
                console.log(response.data)
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    componentDidMount(){
        if(this.props.history.location.state.id){
            this.getInfo(this.props.history.location.state.id)
        }
    }
     

    render() {
        if(this.state.single == null){
            return(<p></p>)
        } else {
            return(
                <Card>
                    <Card.Body>
                        <Row className="mb-4">
                            <Col lg={5}><h2><FaUserMd/> { this.state.single.name }</h2></Col>
                            <Col lg={2}>
                                <Button variant="success" className="btn-block" onClick={this.modalToggle} >Pay Now</Button>
                            </Col>
                            <Col lg={3}>
                                <Link 
                                    to="/manager-admin/view-doctor-payment-history" 
                                    className="btn btn-block btn-secondary">Payment history</Link>
                            </Col>
                            <Col lg={2}>
                                <Button variant="danger" className="btn-block">Disable</Button>
                            </Col>
                        </Row>
    
                        <Row className="mt-3">
                            <Col>
                                <Form.Group>
                                    <Form.Label>Name</Form.Label>
                                    <h3>{ this.state.single.name }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>NIC</Form.Label>
                                    <h3>{ this.state.single.nic }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Gender</Form.Label>
                                    <h3>{ this.state.single.gender }</h3>
                                </Form.Group>
                            </Col>
    
                        </Row>
    
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Address</Form.Label>
                                    <h3>{ this.state.single.address }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Date of birth</Form.Label>
                                    <h3>{ moment(this.state.single.date_of_birth).format('YYYY-MM-DD') }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Contact No</Form.Label>
                                    <h3>{ this.state.single.contact }</h3>
                                </Form.Group>
                            </Col>
    
                        </Row>
    
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Type of unit</Form.Label>
                                    <h3>{ this.state.single.unit_type }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Category</Form.Label>
                                    <h3>{ this.state.single.category }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Qualification</Form.Label>
                                    <h3>{ this.state.single.qualification }</h3>
                                </Form.Group>
                            </Col>
    
                        </Row>
    
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Bank</Form.Label>
                                    <h3>{ this.state.single.bank }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Account Number</Form.Label>
                                    <h3>{ this.state.single.account_number }</h3>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Fee</Form.Label>
                                    <h3>{ this.state.single.fee } LKR</h3>
                                </Form.Group>
                            </Col>
    
                        </Row>
    
    
                        {/* <Row>
                            <Col>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Login information</Card.Title>
                                        <Row>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Email</Form.Label>
                                                    <h3>pragash@gmail.com</h3>
                                                </Form.Group>
                                            </Col>
                                            <Col lg={2}>
                                                <Form.Group>
                                                    <Form.Label>Role</Form.Label>
                                                    <h3>Doctor</h3>
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Password</Form.Label>
                                                    <Form.Control type="password" placeholder="Type a secure password" />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>Confirm Password</Form.Label>
                                                    <Form.Control type="password" placeholder="Confirm password" />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col lg={3}>
                                                <Button variant="info" className="btn-block">Update Password</Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
     */}
    
                        <Modal show={this.state.show_modal} onHide={this.modalToggle}>
                            <Modal.Header closeButton>
                                <Modal.Title><FaReceipt /> Make payment to Pragash</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Table>
                                    <tr>
                                        <td><strong>Pay to</strong></td>
                                        <td>{ this.state.single.name }</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Amount Payable</strong><br/>
                                            <small>Payable amount of past week</small>
                                        </td>
                                        <td>{ this.state.single.fee } LKR</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Bank</strong></td>
                                        <td>{ this.state.single.bank }</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Account number</strong></td>
                                        <td>{ this.state.single.account_number }</td>
                                    </tr>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                            <Button variant="success" onClick={this.modalToggle}>
                                Pay Now
                            </Button>
                            </Modal.Footer>
                        </Modal>
    
                    </Card.Body>
                </Card>
            )
        }
        
    }

}

export default View;


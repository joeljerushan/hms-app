import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';
import { Chart } from 'react-charts'

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';

import axios from 'axios';
import { API_URL } from '../../../Config'

class PatientVisiting extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            chart_data: [
                {
                  label: 'Series 1',
                  data: [
                      ['Ampara', '7'], 
                      ['Batticaloa', '17'], 
                      ['Arayampathy', '12'], 
                      ['Navatkudah', '11'], 
                      ['Chenkalady', '10']]
                },
            ],
            chart_axes: [
                { primary: true, type: 'ordinal', position: 'left' },
                { position: 'bottom', type: 'linear', stacked: true }
            ],
            chart_series: {
                type: 'bar'
            },
            loading: true,
            batticaloa: [],
            ampara: [],
            chenkalady: [],
            kalmunai: [],
        };
    }
    
    componentDidMount(){
        let set = this

        //post request for login
        axios({
            method: 'get',
            url: API_URL + 'report/patient-by-address',
            headers: { 
                'Accept' : 'application/json'
            }
        })
        .then(function (response) {
            if(response.data){
                set.setState({ 
                    batticaloa: response.data.batticaloa,
                    ampara: response.data.ampara,
                    chenkalady: response.data.chenkalady,
                    kalmunai: response.data.kalmunai,
                    loading: false
                })
            } else {
                set.setState({
                    list_e: 'No Data Found.'
                })
            }
        })
        .catch(function (error) {
            console.log(error)
        });
    }

    render() {
        
        if(this.state.loading){
            return(<p></p>)
        } else {
            return(
                <Card>
                    <Card.Body>
                        <Row className="mb-4">
                            <Col lg={9}><h2><FaChartBar /> Patient Visiting Report</h2></Col>
                        </Row>
                        <Row>
                            <Col>
                            <div
                            style={{
                                width: '100%',
                                height: '300px'
                            }}
                            >
                            <Chart data={[
                            {
                                label: 'Series 1',
                                data: [
                                    ['Ampara', this.state.ampara ? this.state.ampara.length : 0 ], 
                                    ['Batticaloa', this.state.batticaloa ? this.state.batticaloa.length : 0], 
                                    ['Chenkalady', this.state.chenkalady ? this.state.chenkalady.length : 0], 
                                    ['Kalmunai', this.state.kalmunai ? this.state.kalmunai.length : 0], 
                                ]}
                            ]} series={this.state.chart_series} axes={this.state.chart_axes} />


                            </div> 
                            
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            )
        }

        
    }

}

export default PatientVisiting;


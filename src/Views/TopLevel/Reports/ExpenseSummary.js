import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';

import PieChart from 'react-minimal-pie-chart';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class ExpenseSummary extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            time_span: 'this_month',
            this_month: [
                { title: 'Pharmacy Purchase', value: 55, color: '#53d769', amount: '55000' },
                { title: 'Staff Salary', value: 25, color: '#fecb2e', amount: '25000' },
                { title: 'Doctor Payment', value: 15, color: '#fc3d39', amount: '15000' },
                { title: 'Others', value: 5, color: '#147efb', amount: '5000' },
            ],
            this_year: [
                { title: 'Pharmacy Purchase', value: 62, color: '#53d769', amount: '62000' },
                { title: 'Staff Salary', value: 27, color: '#fecb2e', amount: '27000' },
                { title: 'Doctor Payment', value: 22, color: '#fc3d39', amount: '22000' },
                { title: 'Others', value: 8, color: '#147efb', amount: '8000' },
            ],
            past_year: [
                { title: 'Pharmacy Purchase', value: 69, color: '#53d769', amount: '69000' },
                { title: 'Staff Salary', value: 42, color: '#fecb2e', amount: '42000' },
                { title: 'Doctor Payment', value: 34, color: '#fc3d39', amount: '34000' },
                { title: 'Others', value: 12, color: '#147efb', amount: '12000' },
            ],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }

    renderChart(time){
        let time_scale

        if(time === 'this_month'){
            time_scale = this.state.this_month
        } 
        if(time === 'this_year'){
            time_scale = this.state.this_year
        }
        if(time === 'past_year'){
            time_scale = this.state.past_year
        }

        return (
            <PieChart
                data={time_scale}
                label
                labelPosition={112}
                labelStyle={{ fontFamily: 'sans-serif', fontSize: '4px' }}
                lengthAngle={360}
                lineWidth={100}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={42}
                rounded={false}
                startAngle={0}
                viewBoxSize={[
                    100,
                    100
                ]}
                animate={ true }
                animationDuration={500}
                animationEasing="ease-out"
            />
        )
    }

    renderAmount(time){
        
        if(time === 'this_month'){
            return( 
                this.state.this_month.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        } 
        if(time === 'this_year'){
            return( 
                this.state.this_year.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        }
        if(time === 'past_year'){
            return( 
                this.state.past_year.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        }
        
    }
    

    render() {
        


        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaChartBar /> Expense Summary Report</h2></Col>
                        <Col>
                            <Form.Control 
                                as="select"
                                onChange={this.handleInputChange}
                                name="time_span">
                                <option value="this_month">This Month</option>
                                <option value="this_year">This Year</option>
                                <option value="past_year">Past Year</option>
                            </Form.Control>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={6}>
                            { this.renderChart(this.state.time_span) }
                        </Col>
                        <Col lg={6}>
                            <Row className="mt-5">
                                <Col><div className="colorBox" style={{ backgroundColor: '#53d769' }}></div></Col>
                                <Col>Pharmacy Purchase</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#fecb2e' }}></div></Col>
                                <Col>Staff Salary</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#fc3d39' }}></div></Col>
                                <Col>Doctor Payment</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#147efb' }}></div></Col>
                                <Col>Others</Col>
                            </Row>

                            <Row className="mt-5">
                                <Col>
                                    <div style={{ 'backgroundColor': '#e6e6e6', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">100000 LKR</h4>
                                        <h6 className="text-center">This Month</h6>
                                    </div>
                                </Col>
                                <Col>
                                    <div style={{ 'backgroundColor': '#fc3d39', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">8000 LKR</h4>
                                        <h6 className="text-center">Loss</h6>
                                    </div>
                                </Col>
                                <Col>
                                    <div style={{ 'backgroundColor': '#e6e6e6', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">92000 LKR</h4>
                                        <h6 className="text-center">Last Month</h6>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-5 text-center">

                        { this.renderAmount(this.state.time_span) }
                        
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default ExpenseSummary;


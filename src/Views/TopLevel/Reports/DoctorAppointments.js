import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';
import { Chart } from 'react-charts'

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class DoctorAppointments extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            chart_data: [
                {
                  label: 'Series 1',
                  data: [
                      ['Pragash Maran', '6'], 
                      ['Craneeth Arunachchalam', '7'], 
                      ['Thushanthan', '5'], 
                      ['Praneetha', '6'], 
                      ['Sujana', '8']]
                },
            ],
            chart_axes: [
                { primary: true, type: 'ordinal', position: 'bottom' },
                { position: 'left', type: 'linear', stacked: true }
              ],
            chart_series: {
                type: 'bar'
              }
        };
    }
    

    render() {
        


        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaChartBar /> Doctor Appointments Report</h2></Col>
                    </Row>
                    <Row>
                        <Col>
                        <div
                        style={{
                            width: '700px',
                            height: '300px'
                        }}
                        >
                        <Chart data={this.state.chart_data} series={this.state.chart_series} axes={this.state.chart_axes} />
                        </div> 
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default DoctorAppointments;


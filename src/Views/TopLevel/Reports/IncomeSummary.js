import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';

import PieChart from 'react-minimal-pie-chart';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class IncomeSummary extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            time_span: 'today',
            today: [
                { title: 'Radiology Charge', value: 6, color: '#53d769', amount: '600' },
                { title: 'Pharmacy Payment', value: 55, color: '#fecb2e', amount: '5500' },
                { title: 'Lab Payment', value: 23, color: '#fc3d39', amount: '2300' },
                { title: 'Appointment', value: 16, color: '#147efb', amount: '1600' },
            ],
            this_week: [
                { title: 'Radiology Charge', value: 16, color: '#53d769', amount: '16000' },
                { title: 'Pharmacy Payment', value: 75, color: '#fecb2e', amount: '75500' },
                { title: 'Lab Payment', value: 33, color: '#fc3d39', amount: '33300' },
                { title: 'Appointment', value: 46, color: '#147efb', amount: '4600' },
            ],
            this_month: [
                { title: 'Radiology Charge', value: 26, color: '#53d769', amount: '26000' },
                { title: 'Pharmacy Payment', value: 82, color: '#fecb2e', amount: '82000' },
                { title: 'Lab Payment', value: 43, color: '#fc3d39', amount: '43000' },
                { title: 'Appointment', value: 62, color: '#147efb', amount: '62000' },
            ],
            this_year: [
                { title: 'Radiology Charge', value: 68, color: '#53d769', amount: '68000' },
                { title: 'Pharmacy Payment', value: 122, color: '#fecb2e', amount: '122000' },
                { title: 'Lab Payment', value: 93, color: '#fc3d39', amount: '93000' },
                { title: 'Appointment', value: 26, color: '#147efb', amount: '26000' },
            ],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }

    renderChart(time){
        let time_scale

        if(time === 'today'){
            time_scale = this.state.today
        } 
        if(time === 'this_week'){
            time_scale = this.state.this_week
        }
        if(time === 'this_month'){
            time_scale = this.state.this_month
        }
        if(time === 'this_year'){
            time_scale = this.state.this_year
        }

        return (
            <PieChart
                data={time_scale}
                label
                labelPosition={112}
                labelStyle={{ fontFamily: 'sans-serif', fontSize: '4px' }}
                lengthAngle={360}
                lineWidth={100}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={42}
                rounded={false}
                startAngle={0}
                viewBoxSize={[
                    100,
                    100
                ]}
                animate={ true }
                animationDuration={500}
                animationEasing="ease-out"
            />
        )
    }

    renderAmount(time){
        
        if(time === 'today'){
            return( 
                this.state.today.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        } 
        if(time === 'this_week'){
            return( 
                this.state.this_week.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        }
        if(time === 'this_month'){
            return( 
                this.state.this_month.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        }
        if(time === 'this_year'){
            return( 
                this.state.this_year.map((single, index) => 
                    <Col key={index}>
                        <div style={{ 'backgroundColor': single.color, borderRadius: 5, }} className="pt-3 pb-3">
                            <h2>{ single.amount } LKR</h2>
                            <h5>{ single.title }</h5>
                        </div>
                    </Col>
                )
            )
        }
    }
    

    render() {
        


        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaChartBar /> Income Summary Report</h2></Col>
                        <Col>
                            <Form.Control 
                                as="select"
                                onChange={this.handleInputChange}
                                name="time_span">
                                <option value="today">Today</option>
                                <option value="this_week">This Week</option>
                                <option value="this_month">This Month</option>
                                <option value="this_year">This Year</option>
                            </Form.Control>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={6}>
                            { this.renderChart(this.state.time_span) }
                        </Col>
                        <Col lg={6}>
                            <Col lg={8}>
                                <Row className="mt-5">
                                    <Col lg={4}><div className="colorBox" style={{ backgroundColor: '#53d769' }}></div></Col>
                                    <Col lg={8}><div className="mt-3">Radiology Charge</div></Col>
                                </Row>
                                <Row>
                                    <Col lg={4}><div className="colorBox" style={{ backgroundColor: '#fecb2e' }}></div></Col>
                                    <Col lg={8}><div className="mt-3">Pharmacy</div></Col>
                                </Row>
                                <Row>
                                    <Col lg={4}><div className="colorBox" style={{ backgroundColor: '#fc3d39' }}></div></Col>
                                    <Col lg={8}><div className="mt-3">Lab Payment</div></Col>
                                </Row>
                                <Row>
                                    <Col lg={4}><div className="colorBox" style={{ backgroundColor: '#147efb' }}></div></Col>
                                    <Col lg={8}><div className="mt-3">Appointment</div></Col>
                                </Row>
                            </Col>

                            <Row className="mt-5">
                                <Col>
                                    <div style={{ 'backgroundColor': '#e6e6e6', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">10000 LKR</h4>
                                        <h6 className="text-center">Today</h6>
                                    </div>
                                </Col>
                                <Col>
                                    <div style={{ 'backgroundColor': '#53d769', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">2000 LKR</h4>
                                        <h6 className="text-center">Profit Increase</h6>
                                    </div>
                                </Col>
                                <Col>
                                    <div style={{ 'backgroundColor': '#e6e6e6', borderRadius: 5, }} className="pt-3 pb-3">
                                        <h4 className="text-center">8000 LKR</h4>
                                        <h6 className="text-center">Yesterday</h6>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-5 text-center">

                        { this.renderAmount(this.state.time_span) }
                        
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default IncomeSummary;


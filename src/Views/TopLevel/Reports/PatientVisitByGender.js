import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';

import PieChart from 'react-minimal-pie-chart';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class PatientVisitByGender extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            pie_chart_data: [
                { title: 'Male', value: 12, color: '#5fc9f8' },
                { title: 'Female', value: 23, color: '#fc3158' },
            ]
        };
    }
    

    render() {
        


        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaChartBar /> Patient visits by gender</h2></Col>
                    </Row>
                    <Row>
                        <Col lg={6}>
                            <PieChart
                                data={this.state.pie_chart_data}
                                label
                                labelPosition={112}
                                labelStyle={{
                                    fontFamily: 'sans-serif',
                                    fontSize: '4px'
                                }}
                                lengthAngle={360}
                                lineWidth={100}
                                onClick={undefined}
                                onMouseOut={undefined}
                                onMouseOver={undefined}
                                paddingAngle={0}
                                radius={42}
                                rounded={false}
                                startAngle={0}
                                viewBoxSize={[
                                    100,
                                    100
                                ]}
                                animate={ true }
                                animationDuration={500}
                                animationEasing="ease-out"
                            />
                        </Col>
                        <Col lg={3}>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#5fc9f8' }}></div></Col>
                                <Col>Male</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#fc3158' }}></div></Col>
                                <Col>Female</Col>
                            </Row>
                            
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default PatientVisitByGender;


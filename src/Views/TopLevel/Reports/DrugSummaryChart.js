import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaChartBar } from 'react-icons/fa';

import PieChart from 'react-minimal-pie-chart';

import { Row, Col, Badge, Button, Form, Card, Table} from 'react-bootstrap';


class DrugSummaryChart extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            chart_data: [
                {
                    label: 'Series 1',
                    data: [
                        ['Panadol', 6], 
                        ['Vitamin C', 15], 
                        ['Amoxicillin', 23], 
                        ['Piriton', 16], 
                        ['Saline', 12]
                    ]
                },
            ],
            chart_axes: [
                { primary: true, type: 'time', position: 'bottom' },
                { type: 'linear', position: 'left' }
              ],
            chart_series: {
                showPoints: true
            },
            pie_chart_data: [
                { title: 'Panadol', value: 6, color: '#5fc9f8' },
                { title: 'Vitamin C', value: 15, color: '#fecb2e' },
                { title: 'Amoxicillin', value: 23, color: '#fc3158' },
                { title: 'Piriton', value: 16, color: '#147efb' },
                { title: 'Saline', value: 12, color: '#53d769' },
            ]
        };
    }
    

    render() {
        


        return(
            <Card>
                <Card.Body>
                    <Row className="mb-4">
                        <Col lg={9}><h2><FaChartBar /> Drug Summary Report</h2></Col>
                    </Row>
                    <Row>
                        <Col lg={6}>
                            <PieChart
                                data={this.state.pie_chart_data}
                                label
                                labelPosition={112}
                                labelStyle={{
                                    fontFamily: 'sans-serif',
                                    fontSize: '4px'
                                }}
                                lengthAngle={360}
                                lineWidth={100}
                                onClick={undefined}
                                onMouseOut={undefined}
                                onMouseOver={undefined}
                                paddingAngle={0}
                                radius={42}
                                rounded={false}
                                startAngle={0}
                                viewBoxSize={[
                                    100,
                                    100
                                ]}
                                animate={ true }
                                animationDuration={500}
                                animationEasing="ease-out"
                            />
                        </Col>
                        <Col lg={3}>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#5fc9f8' }}></div></Col>
                                <Col>Panadol</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#fecb2e' }}></div></Col>
                                <Col>Vitamin C</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#fc3158' }}></div></Col>
                                <Col>Amoxicillin</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#147efb' }}></div></Col>
                                <Col>Piriton</Col>
                            </Row>
                            <Row>
                                <Col><div className="colorBox" style={{ backgroundColor: '#53d769' }}></div></Col>
                                <Col>Saline</Col>
                            </Row>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        )
    }

}

export default DrugSummaryChart;


import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { Container, Row, Col, Button, Form, Badge } from 'react-bootstrap';

import logo from '../../images/hospital-building.png';


import FormData from 'form-data';
import axios from 'axios';
import { API_URL } from '../../Config'

import { reactLocalStorage } from 'reactjs-localstorage';


class Index extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: '',
            password: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    LoginUser(){
        let valid_email = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)

        if(this.state.email == ''){
            this.setState({ email_e: "Email Required." })
        } else if(this.state.password == ''){
            this.setState({ password_e: "Password Required." })
        } else {
            let set = this
            if(valid_email){
                var formData = new FormData(); 
                formData.append('email', this.state.email);
                formData.append('password', this.state.password);

                axios({
                    method: 'post',
                    url: API_URL + 'login',
                    data: formData,
                    headers: { 
                        'Content-Type': 'multipart/form-data',
                    }
                })
                .then(function (response) {
                  try {
                        if(response.data){
                            //self.props.saveLoginUser(response.data)
                            reactLocalStorage.set('logged', true);
                            reactLocalStorage.set('role', response.data.user.role);
                            reactLocalStorage.set('email', response.data.user.email);
                            reactLocalStorage.set('id', response.data.user.id);
                            reactLocalStorage.set('name', response.data.user.name);
                            
                            set.props.history.push('/')
                            window.location.reload();
                        }
                    } catch(e) {
                        set.setState({ email_e: "Logged with errors." })
                    } 
                })
                .catch(function (error) {
                    set.setState({ email_e: "Your email or password not valid." })
                });
            
            } else {
                set.setState({ email_e: "Valid email Required." })
            }
        }
    }

    render() {
        return( 
            <div className="mt-5">
                <Container className="pt-5">
                    <Row className="justify-content-center mt-5">
                        <Col lg={3} className="mt-5">
                            <div className="mt-5 text-center borderRightLogin">
                                <img src={logo} className="logoWidth" />
                            </div>
                        </Col>
                        <Col lg={5} className="mt-5 pl-4">
                            <div>
                                <h2 className="mt-5 mb-4">Asceso Login</h2>
                            </div>
                            
                            <Row>
                                <Col lg={8}>
                                    <Form.Group as={Row} controlId="formPlaintextEmail">
                                        <Col>
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control 
                                                type="email" 
                                                placeholder="Email"
                                                onChange={this.handleInputChange}
                                                name="email" />
                                            { this.state.email_e ? <Badge variant="danger">{ this.state.email_e } </Badge> : ''}
                                        </Col>
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={8}>
                                    <Form.Group as={Row} controlId="formPlaintextEmail">
                                        <Col>
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control 
                                                type="password" 
                                                placeholder="Password"
                                                onChange={this.handleInputChange}
                                                name="password"  />
                                            { this.state.password_e ? <Badge variant="danger">{ this.state.password_e } </Badge> : ''}
                                        </Col>
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={8}>
                                    {
                                        this.state.loading ? 
                                        <Button className="btn btn-secondary btn-block" disabled>Loading</Button>
                                        :
                                        <Button className="btn btn-success btn-block" onClick={() => this.LoginUser() }>Login</Button>
                                    }
                                    
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

}

export default withRouter(Index);
import React, { Component } from 'react';

import { Link, withRouter } from "react-router-dom";
import { FaUsers, FaUserMd, FaUserSecret, FaTable, FaUserFriends, FaWheelchair, FaChartBar } from 'react-icons/fa';
import { Row, Col, Badge, Button, Form, ListGroup, Card} from 'react-bootstrap';


class MenuGenerate extends Component { 
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        if(this.props.role == "manager_admin"){ // Manager Admin - 13
            return(
                <div className="sidebar-sticky">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <Link to="/manager-admin/manage-doctors" className="nav-link"><FaUserMd/> Doctor Management</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/manager-admin/manage-staff" className="nav-link"><FaUsers/> Staff Management</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/manager-admin/manage-auth-users" className="nav-link"><FaUserSecret/> Manage Auth Users</Link>
                        </li>
                        {/* <li className="nav-item">
                            <Link to="/manager-admin/view-weekly-income" className="nav-link"><FaTable/> Weekly Income</Link>
                        </li> */}
                    </ul>
                </div>
            )
        } else if (this.props.role == "front_office_clerk"){
            return(
                <div className="sidebar-sticky">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <Link to="/front-office-clerk/patients" className="nav-link"><FaUserFriends /> Patients</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/front-office-clerk/appointments" className="nav-link"><FaWheelchair /> Appointments</Link>
                        </li>
                    </ul>
                </div>
            )
        } else if (this.props.role == "top_level_management"){
            return(
                <div className="sidebar-sticky">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <Link to="/top-level/reports/patient-visiting" className="nav-link">
                                <FaChartBar /> Patient Visiting Report
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/top-level/reports/patient-visiting-by-gender" className="nav-link">
                                <FaChartBar /> Patient Visiting by Gender Report
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/top-level/reports/doctor-appointments" className="nav-link">
                                <FaChartBar /> Doctor Appointments Report
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/top-level/reports/drug-summary" className="nav-link">
                                <FaChartBar /> Drug Summary Report
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/top-level/reports/income-summary" className="nav-link">
                                <FaChartBar /> Income Summary
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/top-level/reports/expense-summary" className="nav-link">
                                <FaChartBar /> Expense Summary
                            </Link>
                        </li>
                        
                    </ul>
                </div>
            )
        } else { 
            return( <span></span> )
        }
    }

}

export default MenuGenerate;
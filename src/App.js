import React from 'react';
import AppRouter from './Router'
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

import { FaSpinner } from 'react-icons/fa';
//import redix and provider
import { Provider } from 'react-redux';
//presist
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './Redux';


function App() {
  return (
    <div>
    <Router>
    <AppRouter />
    </Router>
      
    </div>
  );
}

export default App;
